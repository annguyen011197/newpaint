#pragma once
#include "CShape.h"
class CCircle :
	public CShape
{
public:
	CCircle();
	void Draw(Graphics * graphics);
	void Set(Point _fP, Point _lP, Pen *_pen);
	~CCircle();
};

