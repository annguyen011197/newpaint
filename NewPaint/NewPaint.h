#pragma once

#include "resource.h"
#include <windowsx.h>
#include <vector>
#include <ObjIdl.h>
#include <gdiplus.h>
#pragma comment(lib,"gdiplus.lib")
#include <objbase.h>
#pragma comment(lib, "Ole32.lib")
#include <commctrl.h>
#pragma comment(lib, "ComCtl32.lib")
#include <Commdlg.h>
#include "CShape.h"
#include "CLine.h"
#include "CRectangle.h"
#include "CCircle.h"
#include "RibbonFramework.h"
#include "RibbonIDs.h"
#define MAX_LOADSTRING 100
using namespace Gdiplus;

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

int x, y, width, height, selectShape;
bool checkFlag = 0;
Point fPoint(0, 0);
Point rPoint(0, 0);
Point lPoint(100, 100);
bool gIsDrawing = 0;
RECT rect;
std::vector<CShape*> shape;
int lineWidth = 1;
COLORREF selectedColor;
Pen* pen = new Pen(Color(255, 0, 0, 0), lineWidth);
HDC hdcMem;
HBITMAP hbmMem;
HANDLE hOld;
Graphics* gr;
HWND editHwnd;
WNDPROC editProc;
bool gShift;
Image* img = NULL;


												// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

//Handle Msg
BOOL OnCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hwnd);
void OnLButtonDown(HWND hwnd, bool doubleClick, int lword, int hword, UINT code);
void OnMouseMove(HWND hwnd, int lword, int hword, UINT code);
void OnLButtonUp(HWND hwnd, int lword, int hword, UINT code);
void OnDestroy(HWND hwnd);
void OnSize(HWND hwnd, UINT state, int cx, int cy);
void OnKeyDown(HWND hwnd, UINT key, bool check, UINT repeat, UINT flags);


//Function
LRESULT CALLBACK EditProcedure(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void UpdatePoint(Point &p, int x, int y);
PBITMAPINFO CreateBitMapInfo(HWND hwnd, HBITMAP hBmp);
void CreateBMPFile(HWND hwnd, LPTSTR pszFile, PBITMAPINFO pbi,
	HBITMAP hBMP, HDC hDC);
bool SaveFile(HWND hwnd);
bool LoadFile(HWND hwnd);