#include "stdafx.h"
#include "CCircle.h"


CCircle::CCircle()
{
}

void CCircle::Draw(Graphics * graphics)
{
	graphics->DrawEllipse(this->pen, this->fP.X, this->fP.Y, abs(this->lP.X - this->fP.X), abs(this->lP.Y - this->fP.Y));
}

void CCircle::Set(Point _fP, Point _lP, Pen * _pen)
{
	fP = _fP;
	lP = _lP;
	pen = _pen;
}


CCircle::~CCircle()
{
}
