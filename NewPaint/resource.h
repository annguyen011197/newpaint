//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by NewPaint.rc
//
#define IDC_MYICON                      2
#define IDD_NEWPAINT_DIALOG             102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_NEWPAINT                    107
#define IDI_SMALL                       108
#define IDC_NEWPAINT                    109
#define IDR_MAINFRAME                   128
#define ID_BTN_SIZE_1                   129
#define ID_BTN_SIZE_2                   130
#define ID_BTN_SIZE_3                   131
#define ID_FILE_SAVE                    32771
#define IDC_STATIC                      -1
#define M_SAVE							132
#define M_LOAD							133

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
