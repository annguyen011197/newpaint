#pragma once
#include "CShape.h"
class CLine : public CShape
{
public:
	CLine();
	void Draw(Graphics * graphics);
	void Set(Point _fP, Point _lP, Pen *_pen);
	~CLine();
};

