#include "stdafx.h"
#include "CLine.h"


CLine::CLine() : CShape()
{
}

void CLine::Draw(Graphics * graphics)
{
	graphics->DrawLine(this->pen, this->fP, this->lP);
}

void CLine::Set(Point _fP, Point _lP, Pen * _pen)
{
	fP = _fP;
	lP = _lP;
	pen = _pen;
}


CLine::~CLine()
{
}
