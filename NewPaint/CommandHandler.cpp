﻿// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
#include "stdafx.h"
#include <UIRibbon.h>
#include "CommandHandler.h"
#include "RibbonIDs.h"
#include "resource.h"
#include "RibbonFramework.h"
#include "PropertySet.h"
#include <UIRibbonPropertyHelpers.h>
#include <string>
#pragma comment(lib, "propsys.lib")


 
// Static method to create an instance of the object.
HRESULT CCommandHandler::CreateInstance(IUICommandHandler **ppCommandHandler)
{
	if (!ppCommandHandler)
	{
		return E_POINTER;
	}

	*ppCommandHandler = NULL;

	HRESULT hr = S_OK;

	CCommandHandler* pCommandHandler = new CCommandHandler();

	if (pCommandHandler != NULL)
	{
		*ppCommandHandler = static_cast<IUICommandHandler *>(pCommandHandler);
	}
	else
	{
		hr = E_OUTOFMEMORY;
	}

	return hr;
}

// IUnknown method implementations.
STDMETHODIMP_(ULONG) CCommandHandler::AddRef()
{
	return InterlockedIncrement(&m_cRef);
}

STDMETHODIMP_(ULONG) CCommandHandler::Release()
{
	LONG cRef = InterlockedDecrement(&m_cRef);
	if (cRef == 0)
	{
		delete this;
	}

	return cRef;
}

STDMETHODIMP CCommandHandler::QueryInterface(REFIID iid, void** ppv)
{
	if (iid == __uuidof(IUnknown))
	{
		*ppv = static_cast<IUnknown*>(this);
	}
	else if (iid == __uuidof(IUICommandHandler))
	{
		*ppv = static_cast<IUICommandHandler*>(this);
	}
	else
	{
		*ppv = NULL;
		return E_NOINTERFACE;
	}

	AddRef();
	return S_OK;
}

//
//  FUNCTION: UpdateProperty()
//
//  PURPOSE: Called by the Ribbon framework when a command property (PKEY) needs to be updated.
//
//  COMMENTS:
//
//    This function is used to provide new command property values, such as labels, icons, or
//    tooltip information, when requested by the Ribbon framework.  
//    
//    In this SimpleRibbon sample, the method is not implemented.  
//
STDMETHODIMP CCommandHandler::UpdateProperty(
	UINT nCmdID,
	REFPROPERTYKEY key,
	const PROPVARIANT* ppropvarCurrentValue,
	PROPVARIANT* ppropvarNewValue)
{
	UNREFERENCED_PARAMETER(nCmdID);
	UNREFERENCED_PARAMETER(key);
	UNREFERENCED_PARAMETER(ppropvarCurrentValue);
	UNREFERENCED_PARAMETER(ppropvarNewValue);
	HRESULT hr = E_FAIL;
	if (key == UI_PKEY_ItemsSource && nCmdID==ID_BTN_SIZE)
	{
		IUICollection* pCollection;
		hr = ppropvarCurrentValue->punkVal->QueryInterface(IID_PPV_ARGS(&pCollection));
		if (FAILED(hr))
		{
			return hr;
		}

		int labelIds[] = { 1,2,3 };

		for (int i = 0; i<_countof(labelIds); i++)
		{
			CPropertySet* pItem;
			hr = CPropertySet::CreateInstance(&pItem);
			//if (FAILED(hr))
			//{
			//	pCollection->Release();
			//	return hr;
			//}
			pItem->InitializeItemProperties(NULL, std::to_wstring(labelIds[i]).c_str(), UI_COLLECTION_INVALIDINDEX);
			pCollection->Add(pItem);
			pItem->Release();

		}
		pCollection->Release();
		hr = S_OK;
	}
}

//
//  FUNCTION: Execute()
//
//  PURPOSE: Called by the Ribbon framework when a command is executed by the user.  For example, when
//           a button is pressed.
//
STDMETHODIMP CCommandHandler::Execute(
	UINT nCmdID,
	UI_EXECUTIONVERB verb,
	const PROPERTYKEY* key,
	const PROPVARIANT* ppropvarValue,
	IUISimplePropertySet* pCommandExecutionProperties)
{
	UNREFERENCED_PARAMETER(pCommandExecutionProperties);
	UNREFERENCED_PARAMETER(ppropvarValue);
	UNREFERENCED_PARAMETER(key);
	UNREFERENCED_PARAMETER(verb);
	UNREFERENCED_PARAMETER(nCmdID);
	if (verb == UI_EXECUTIONVERB_EXECUTE) {
		switch (nCmdID) {
		case ID_BTN_LINE:
		{
			setData(0, ID_BTN_LINE);
			selectShape = 0;
			CheckToggleButton(ID_BTN_RECTANGLE, 0);
			CheckToggleButton(ID_BTN_CIRCLE, 0);
			break;
		}
		case ID_BTN_RECTANGLE:
			setData(0, ID_BTN_RECTANGLE);
			selectShape = 1;
			CheckToggleButton(ID_BTN_LINE, 0);
			CheckToggleButton(ID_BTN_CIRCLE, 0);
			break;
		case ID_BTN_CIRCLE:
			setData(0, ID_BTN_CIRCLE);
			selectShape = 2;
			CheckToggleButton(ID_BTN_LINE, 0);
			CheckToggleButton(ID_BTN_RECTANGLE, 0);
			break;
		case ID_BTN_SIZE:
		{
			switch (ppropvarValue->uintVal) {
			case 1:
			case 2:
			case 3:
				lineWidth = ppropvarValue->uintVal;
				break;
			case UI_COLLECTION_INVALIDINDEX:
			{
				PROPVARIANT var;
				pCommandExecutionProperties->GetValue(UI_PKEY_Label, &var); // The text entered by the user is contained in the property set with the pkey UI_PKEY_Label.

				BSTR bstr = var.bstrVal;
				ULONG newSize;

				HRESULT hr = VarUI4FromStr(bstr, GetUserDefaultLCID(), 0, &newSize);
				lineWidth = newSize;
				PropVariantClear(&var);
			break;
			}
			}
			break;
		}
		case IDR_CMD_THEMEDDCP:
		{
			UINT type = UI_SWATCHCOLORTYPE_NOCOLOR;
			HRESULT hr;
			UINT color = 0;
			if (ppropvarValue != NULL)
			{
				// Retrieve color type.
				hr = UIPropertyToUInt32(UI_PKEY_ColorType, *ppropvarValue, &type);
				if (FAILED(hr))
				{
					return hr;
				}
			}

			// The Ribbon framework passes color as additional property if the color type is RGB.
			if (type == UI_SWATCHCOLORTYPE_RGB && pCommandExecutionProperties != NULL)
			{
				// Retrieve color.
				PROPVARIANT var;
				hr = pCommandExecutionProperties->GetValue(UI_PKEY_Color, &var);
				if (FAILED(hr))
				{
					return hr;
				}
				UIPropertyToUInt32(UI_PKEY_Color, var, &color);
			}
			selectedColor = (COLORREF)color;
			break;
		}
		case ID_BTN_SAVE:
		{
			SendMessage(editHwnd, WM_USER, M_SAVE, 0);
			break;
		}
		case ID_BTN_LOAD:
		{
			SendMessage(editHwnd, WM_USER, M_LOAD, 0);
			break;
		}
		}
	}
	return S_OK;
}

