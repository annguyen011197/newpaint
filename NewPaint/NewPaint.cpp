// NewPaint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "NewPaint.h"


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
	CoInitialize(NULL);
    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_NEWPAINT, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_NEWPAINT));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
	CoUninitialize();
    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= 0;// CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_NEWPAINT));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_NEWPAINT);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }
   else {
	   x = 0;
	   y = 150;
	   width = 1000;
	   height = 500;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//

GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, OnLButtonDown);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, OnMouseMove);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, OnLButtonUp);
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		HANDLE_MSG(hWnd, WM_KEYDOWN, OnKeyDown);
		HANDLE_MSG(hWnd, WM_KEYUP, OnKeyDown);
	case WM_PAINT: {
		OnPaint(hWnd);
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}
	case WM_ERASEBKGND:	
	{
		return TRUE;
		break;
	}
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct)
{
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	GetClientRect(hwnd, &rect);
	InitializeFramework(hwnd);
	int width = rect.right - rect.left;
	int height = rect.bottom - rect.top;
	pen = new Pen(Color(GetRValue(selectedColor), GetGValue(selectedColor), GetBValue(selectedColor)), lineWidth);
	editHwnd = CreateWindow(L"static", L"", WS_CHILD | WS_VISIBLE , x, y, width, height, hwnd, NULL, hInst, NULL);
	editProc = (WNDPROC)SetWindowLongPtr(editHwnd, GWLP_WNDPROC, (LONG_PTR)&EditProcedure);
	return 1;
}

void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
	{
		// Parse the menu selections:
		switch (id)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hwnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hwnd);
			break;
		}
	}

}

void OnPaint(HWND hwnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(editHwnd, &ps);
	hdcMem = CreateCompatibleDC(hdc);
	hbmMem = CreateCompatibleBitmap(hdc, width, height);
	hOld = SelectObject(hdcMem, hbmMem);
	Graphics* graphics = new Graphics(hdc);
	Graphics* gr = new Graphics(hdcMem);
	gr->Clear(Color::White);
	if (img != NULL) {
		gr->DrawImage(img, 0, 0);
	}
	for (auto i : shape) {
		i->Draw(gr);
	}
	RECT editRect;
	GetClientRect(editHwnd, &editRect);
	BitBlt(hdc, 0, 0, width, height, hdcMem, 0, 0, SRCCOPY);
	if (gIsDrawing) {
		switch (selectShape) {
		case 0:
			graphics->DrawLine(pen, fPoint, lPoint);
			break;
		case 1:
			graphics->DrawRectangle(pen, fPoint.X, fPoint.Y, abs(lPoint.X - fPoint.X), abs(lPoint.Y - fPoint.Y));
			break;
		case 2:
			graphics->DrawEllipse(pen, fPoint.X, fPoint.Y, abs(lPoint.X - fPoint.X), abs(lPoint.Y - fPoint.Y));
			break;
		}
	}
	graphics->~Graphics();
	gr->~Graphics();
	delete graphics;
	delete gr;
	DeleteObject(hbmMem);
	DeleteDC(hdcMem);
	EndPaint(editHwnd, &ps);

}

void OnLButtonDown(HWND hwnd, bool doubleClick, int lword, int hword, UINT code)
{
	if (!doubleClick) {
		UpdatePoint(fPoint, lword, hword);
		rPoint.X = lword;
		rPoint.Y = hword;
		gIsDrawing = 1;
		pen = new Pen(Color(GetRValue(selectedColor),GetGValue(selectedColor),GetBValue(selectedColor)), lineWidth);
		//pen->SetColor(Color(GetRValue(selectedColor), GetGValue(selectedColor), GetBValue(selectedColor)));
		//pen->SetWidth(lineWidth);
	}
}

void OnMouseMove(HWND hwnd, int cx, int cy, UINT code)
{
	int recW, recH, nX, nY, rec;
	if (gIsDrawing) {
		switch (selectShape) {
		case 0:
			UpdatePoint(lPoint, cx, cy);
			break;
		case 1:
		{
			if (!gShift) {
				 recW = abs(cx - rPoint.X);
				 recH = abs(cy - rPoint.Y);
				 nX = rPoint.X > cx ? cx : rPoint.X;
				 nY = rPoint.Y > cy ? cy : rPoint.Y;
				UpdatePoint(fPoint, nX, nY);
				UpdatePoint(lPoint, nX + recW, nY + recH);
			}
			else {
				 recW = abs(cx - rPoint.X);
				 recH = abs(cy - rPoint.Y);
				 rec = recW > recH ? recW : recH;
				 nX = rPoint.X > cx ? rPoint.X-rec : rPoint.X;
				 nY = rPoint.Y > cy ? rPoint.Y-rec : rPoint.Y;
				UpdatePoint(fPoint, nX, nY);
				UpdatePoint(lPoint, nX + rec, nY + rec);
			}
			break;
		}
		case 2:
		{
			if (!gShift) {
				 recW = abs(cx - rPoint.X);
				 recH = abs(cy - rPoint.Y);
				 nX = rPoint.X > cx ? cx : rPoint.X;
				 nY = rPoint.Y > cy ? cy : rPoint.Y;
				UpdatePoint(fPoint, nX, nY);
				UpdatePoint(lPoint, nX + recW, nY + recH);
			}
			else {
				 recW = abs(cx - rPoint.X);
				 recH = abs(cy - rPoint.Y);
				 rec = recW > recH ? recW : recH;
				 nX = rPoint.X > cx ? rPoint.X - rec : rPoint.X;
				 nY = rPoint.Y > cy ? rPoint.Y - rec : rPoint.Y;
				UpdatePoint(fPoint, nX, nY);
				UpdatePoint(lPoint, nX + rec, nY + rec);
			}
			break;
		}
		}
		InvalidateRect(hwnd, &rect, FALSE);
	}
}

void OnLButtonUp(HWND hwnd, int lword, int hword, UINT code)
{
	CShape* newShape = NULL;
	switch (selectShape) {
	case 0:
		newShape = new CLine;
		break;
	case 1:
		newShape = new CRectangle;
		break;
	case 2:
		newShape = new CCircle;
		break;
	}
	newShape->Set(fPoint, lPoint, pen);
	shape.push_back(newShape);
	gIsDrawing = 0;
	InvalidateRect(hwnd, &rect, FALSE);
}

void OnDestroy(HWND hwnd)
{
	delete img;
	DestroyFramework();
	GdiplusShutdown(gdiplusToken);
	PostQuitMessage(0);
}

void OnSize(HWND hwnd, UINT state, int cx, int cy)
{
	GetClientRect(hwnd, &rect);
	HBRUSH brush = CreateSolidBrush(RGB(225, 225, 225));
	FillRect(GetDC(hwnd), &rect, brush);
	InitializeFramework(hwnd);
	MoveWindow(editHwnd, x, y, width, height, NULL);
	InvalidateRect(hwnd, &rect, FALSE);
}

void OnKeyDown(HWND hwnd, UINT key, bool check, UINT repeat, UINT flags)
{
	if (check) {
		if (key == VK_LSHIFT || key == VK_RSHIFT || key == VK_SHIFT) {
			gShift = 1;
		}
	}
	else {
		if (key == VK_LSHIFT || key == VK_RSHIFT || key == VK_SHIFT) {
			gShift = 0;
		}
	}
}

LRESULT CALLBACK EditProcedure(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_USER) {
		if (wParam == M_SAVE) {
			SaveFile(hWnd);
		}
		if (wParam == M_LOAD) {
			LoadFile(hWnd);
		}
	}
	return CallWindowProc(editProc, hWnd, uMsg, wParam, lParam);
}

void UpdatePoint(Point &p, int _x, int _y)
{
	p.X = _x;
	p.Y = _y-y;
}

PBITMAPINFO CreateBitMapInfo(HWND hwnd, HBITMAP hBmp) {
	BITMAP bmp;
	PBITMAPINFO pbmi = NULL;
	WORD cClrBits;
	GetObject(hBmp, sizeof(BITMAP), &bmp);

	cClrBits = (WORD)(bmp.bmPlanes * bmp.bmBitsPixel);
	if (cClrBits == 1)
		cClrBits = 1;
	else if (cClrBits <= 4)
		cClrBits = 4;
	else if (cClrBits <= 8)
		cClrBits = 8;
	else if (cClrBits <= 16)
		cClrBits = 16;
	else if (cClrBits <= 24)
		cClrBits = 24;
	else cClrBits = 32;

	pbmi = cClrBits < 24 ?
		(PBITMAPINFO)LocalAlloc(LPTR,
			sizeof(BITMAPINFOHEADER) +
			sizeof(RGBQUAD) * (1 << cClrBits)) :
			(PBITMAPINFO)LocalAlloc(LPTR,
				sizeof(BITMAPINFOHEADER));

	pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	pbmi->bmiHeader.biWidth = bmp.bmWidth;
	pbmi->bmiHeader.biHeight = bmp.bmHeight;
	pbmi->bmiHeader.biPlanes = bmp.bmPlanes;
	pbmi->bmiHeader.biBitCount = bmp.bmBitsPixel;
	cClrBits < 24 ? pbmi->bmiHeader.biClrUsed = (1 << cClrBits) : 0;
	pbmi->bmiHeader.biCompression = BI_RGB;
	pbmi->bmiHeader.biSizeImage = ((pbmi->bmiHeader.biWidth * cClrBits + 31) & ~31) / 8
		* pbmi->bmiHeader.biHeight;
	pbmi->bmiHeader.biClrImportant = 0;
	return pbmi;
}

void CreateBMPFile(HWND hwnd, LPTSTR pszFile, PBITMAPINFO pbi,
	HBITMAP hBMP, HDC hDC)
{
	HANDLE hf;                 // file handle  
	BITMAPFILEHEADER hdr;       // bitmap file-header  
	PBITMAPINFOHEADER pbih;     // bitmap info-header  
	LPBYTE lpBits;              // memory pointer  
	DWORD dwTotal;              // total count of bytes  
	DWORD cb;                   // incremental count of bytes  
	BYTE *hp;                   // byte pointer  
	DWORD dwTmp;

	pbih = (PBITMAPINFOHEADER)pbi;
	lpBits = (LPBYTE)GlobalAlloc(GMEM_FIXED, pbih->biSizeImage);

	if (!GetDIBits(hDC, hBMP, 0, (WORD)pbih->biHeight, lpBits, pbi,
		DIB_RGB_COLORS))
	{
		MessageBox(editHwnd, L"Error", L"Something wrong", NULL);
		return;
	}

	// Create the .BMP file.  
	hf = CreateFile(pszFile,
		GENERIC_READ | GENERIC_WRITE,
		(DWORD)0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		(HANDLE)NULL);
	if (hf == INVALID_HANDLE_VALUE) {
		MessageBox(editHwnd, L"Error", L"Cannot Create File", NULL);
		return;
	}
	hdr.bfType = 0x4d42;        // 0x42 = "B" 0x4d = "M"  
								// Compute the size of the entire file.  
	hdr.bfSize = (DWORD)(sizeof(BITMAPFILEHEADER) +
		pbih->biSize + pbih->biClrUsed
		* sizeof(RGBQUAD) + pbih->biSizeImage);
	hdr.bfReserved1 = 0;
	hdr.bfReserved2 = 0;

	// Compute the offset to the array of color indices.  
	hdr.bfOffBits = (DWORD) sizeof(BITMAPFILEHEADER) +
		pbih->biSize + pbih->biClrUsed
		* sizeof(RGBQUAD);

	// Copy the BITMAPFILEHEADER into the .BMP file.  
	if (!WriteFile(hf, (LPVOID)&hdr, sizeof(BITMAPFILEHEADER),
		(LPDWORD)&dwTmp, NULL))
	{
		MessageBox(editHwnd, L"Error", L"Cannot Write File", NULL);
		return;
	}

	// Copy the BITMAPINFOHEADER and RGBQUAD array into the file.  
	if (!WriteFile(hf, (LPVOID)pbih, sizeof(BITMAPINFOHEADER)
		+ pbih->biClrUsed * sizeof(RGBQUAD),
		(LPDWORD)&dwTmp, (NULL))) {
		MessageBox(editHwnd, L"Error", L"Cannot Write File", NULL);
		return;
	}

	// Copy the array of color indices into the .BMP file.  
	dwTotal = cb = pbih->biSizeImage;
	hp = lpBits;
	if (!WriteFile(hf, (LPSTR)hp, (int)cb, (LPDWORD)&dwTmp, NULL)) {
		MessageBox(editHwnd, L"Error", L"Cannot Write File", NULL);
		return;
	}

	// Close the .BMP file.  
	if (!CloseHandle(hf)) {
		MessageBox(editHwnd, L"Error", L"Cannot Close File", NULL);
		return;
	}
	// Free memory.  
	GlobalFree((HGLOBAL)lpBits);
}

bool LoadFile(HWND hwnd) {
	OPENFILENAME ofn;
	WCHAR szFileName[MAX_PATH] = L"";

	ZeroMemory(&ofn, sizeof(ofn));

	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFilter = L"Bitmap Files (*.bmp)\0*.bmp\0\0";
	ofn.lpstrFile = szFileName;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_EXPLORER;
	ofn.lpstrDefExt = L"txt";
	if (GetOpenFileName(&ofn)) {
		img = new Image(szFileName);
		shape.resize(0);
		Graphics *gr = new Graphics(editHwnd);
		gr->DrawImage(img, 0, 0);
	}
	return true;
}
bool SaveFile(HWND hwnd)
{
	OPENFILENAME ofn;
	WCHAR szFileName[MAX_PATH] = L"";

	ZeroMemory(&ofn, sizeof(ofn));

	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFilter = L"Bitmap Files (*.bmp)\0*.bmp\0\0";
	ofn.lpstrFile = szFileName;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_EXPLORER;
	ofn.lpstrDefExt = L"txt";
	if (GetSaveFileName(&ofn)) {
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(editHwnd, &ps);
		hdcMem = CreateCompatibleDC(hdc);
		hbmMem = CreateCompatibleBitmap(hdc, width, height);
		hOld = SelectObject(hdcMem, hbmMem);
		Graphics* gr = new Graphics(hdcMem);
		gr->Clear(Color::White);
		if (img != NULL) {
			gr->DrawImage(img, 0, 0);
		}
		for (auto i : shape) {
			i->Draw(gr);
		}
		RECT editRect;
		BitBlt(hdc, 0, 0, width, height, hdcMem, 0, 0, SRCCOPY);
		PBITMAPINFO pBmi = CreateBitMapInfo(hwnd, hbmMem);
		if (pBmi == NULL) {
			MessageBox(editHwnd, L"Error", L"Something wrong", NULL);
		}
		CreateBMPFile(hwnd, szFileName, pBmi, hbmMem, hdc);
		delete gr;
		DeleteObject(hbmMem);
		DeleteDC(hdcMem);
		EndPaint(editHwnd, &ps);
	}
	return true;
}


