#pragma once
#include <ObjIdl.h>
#include <gdiplus.h>
#pragma comment(lib, "Gdiplus.lib")
using namespace Gdiplus;
class CShape
{
protected:
	Point fP, lP;
	Pen *pen;
public:
	CShape();
	virtual void Draw(Graphics * graphics) = 0;
	virtual void Set(Point _fP, Point _lP, Pen *_pen) = 0;
	virtual ~CShape() = default;
};

