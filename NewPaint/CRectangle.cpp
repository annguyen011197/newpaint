#include "stdafx.h"
#include "CRectangle.h"


CRectangle::CRectangle() : CShape()
{
}

void CRectangle::Draw(Graphics * graphics)
{
	graphics->DrawRectangle(this->pen, this->fP.X, this->fP.Y, abs(this->lP.X - this->fP.X),abs(this->lP.Y - this->fP.Y));
}

void CRectangle::Set(Point _fP, Point _lP, Pen * _pen)
{
	fP = _fP;
	lP = _lP;
	pen = _pen;
}


CRectangle::~CRectangle()
{
}
