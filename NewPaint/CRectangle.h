#pragma once
#include "CShape.h"
class CRectangle : public CShape
{
public:
	CRectangle();
	void Draw(Graphics * graphics);
	void Set(Point _fP, Point _lP, Pen *_pen);
	~CRectangle();
};

